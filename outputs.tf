output "key_ring" {
  value = google_kms_key_ring.default.name
}

output "location" {
  value = google_kms_key_ring.default.location
}

output "keys" {
  value = google_kms_crypto_key.default.*.name
}