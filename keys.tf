resource "google_kms_crypto_key" "default" {
  count = length(var.keys)

  name            = var.keys[count.index].name
  purpose         = var.keys[count.index].purpose
  key_ring        = google_kms_key_ring.default.self_link
  rotation_period = var.rotation_period

  lifecycle {
    prevent_destroy = true
  }
}
