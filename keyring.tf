resource "google_kms_key_ring" "default" {
  project  = var.project
  name     = var.name
  location = var.location
}