variable "project" {}
variable "name" {}
variable "location" {}
variable "keys" { type = list(object({ name = string, purpose = string })) }
variable "rotation_period" {
  default = "100000s"
}
